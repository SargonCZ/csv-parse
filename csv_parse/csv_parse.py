'''
CSV parser module
'''
__all__ = ['parse_csv']


def parse_csv(filename, sep=','):
    '''Parse a single line string

    Args:
        filename: full path to file
        sep: separator

    Returns:
        Parsed file contents as a list of lists
    '''

    raise NotImplementedError('This is not implemented yet :-|')
